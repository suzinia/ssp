% =============================================
% Plot spectrogram 
% =============================================
%
% Last modified in Apr. 2017 
% Soo Jin Park (sj.park@ucla.edu)
%
% Example Usage (1)
% >> [sound, Fs] = audioread( <soundFilePath> );
% >> func_spectrogram(sound, Fs);
%
% Example Usage (2)
% >> [sound, Fs] = audioread( <soundFilePath> );
% >> params = struct;
% >> params.NFFT = 2048;          % 2048 FFT bins
% >> params.windowLength = 0.050; % 50-msec window
% >> figure
% >> ax1 = subplot(2,1,1);
% >> func_spectrogram(sound, Fs, params, ax1);
% =============================================

function h=func_spectrogram(sound, Fs, params, ax)

%%
if nargin<4
    ax=axes;
end

if nargin<3
    params = struct;
end

    

%%
% -----------------------
% set default parameters
% -----------------------
if ~isfield(params, 'windowLength')
    windowLength = 0.020;   %[sec]
else
    windowLength = params.windowLength;
end

if ~isfield(params, 'frameInterval')
    frameInterval = 0.001;  %[sec]
else
    frameInterval = params.frameInterval;
end

if ~isfield(params, 'NFFT')
    NFFT = 2^(1+floor(log(windowLength*Fs)/log(2)));
else
    NFFT = params.NFFT;
end

if ~isfield(params, 'maxFreq')
    maxFreq = 8000;
else
    maxFreq = params.maxFreq;
end

if ~isfield(params, 'preemph')
    preemph = 0.97;
else
    preemph = params.preemph;
end




%%
WINspec =  round(windowLength * Fs);
STEPspec = round(frameInterval * Fs);
nOverlap = WINspec-STEPspec;

%%
axes(ax);

[S,F,T] = spectrogram(filter([1 -preemph],1,sound), hamming(WINspec), nOverlap, NFFT, Fs);

maxFreqBin = round(maxFreq *NFFT/Fs);

h = imagesc(T,F(1:maxFreqBin), 20*log(abs(S(1:maxFreqBin,:))));

cmap =colormap('gray');
colormap(flipud(cmap));
axis xy

ylim([0 maxFreq])
ylabel('[Hz]');
xlabel('[sec]');
axis tight;

end